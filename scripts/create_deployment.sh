#!/bin/bash
# Create the master ostree rootfs (ostree sysroot), containing an initial
# deployment. See: https://ostree.readthedocs.io/en/latest/manual/deployment/

# shellcheck disable=SC2155
declare -r MKOSTREE="$(realpath "$(dirname "$(dirname "$0")")")"

declare -r REPO="$MKOSTREE/repo"
declare -r OSNAME="ct-os"
declare -r BOOTLOADER="syslinux"
# The ostree remote URL in installed configuration
declare -r OSTREE_REMOTE_NAME="ct-origin"
declare -r OSTREE_REMOTE_URL="http://10.35.4.91:8000/repo"
declare -r OSTREE_BRANCH="codethink"

# Create the ostree sysroot
declare -r OSTREE_SYSROOT=$(mktemp -d -p /var/tmp ostree-deploy.XXXXXXXXXX)
declare -r OSTREE_SYSROOT_REPO="$OSTREE_SYSROOT/ostree/repo"
declare -r OSTREE_SYSROOT_BOOT="$OSTREE_SYSROOT/boot"

pr_err()
{
	>&2 echo "ERROR: $*"
}

setup()
{
	if [ ! -d "$OSTREE_SYSROOT" ]; then
		pr_err "OSTREE_SYSROOT is not a directory $OSTREE_SYSROOT"
		return 1
	fi
}

# Initialise the ostree sysroot
ostree_sysroot_init()
{
	# TODO use --modern. https://github.com/ostreedev/ostree/pull/1894
	#ostree admin init-fs "$OSTREE_SYSROOT" --modern
	ostree admin init-fs "$OSTREE_SYSROOT"
	ostree admin os-init "$OSNAME" --sysroot="$OSTREE_SYSROOT"

	# Add a remote to the ostree sysroot, so we can pull on the target
	ostree --repo="$OSTREE_SYSROOT_REPO" remote add \
		"$OSTREE_REMOTE_NAME" "$OSTREE_REMOTE_URL" "$OSTREE_BRANCH" \
		--no-gpg-verify
}

# Pull from the local repo we have recently commited too. This is our initial
# deployment.
ostree_sysroot_pull()
{
	ostree --repo="$OSTREE_SYSROOT_REPO" pull-local \
		"$REPO" "$OSTREE_BRANCH" --remote="$OSTREE_REMOTE_NAME" \
		--disable-fsync
}

# ostree deploy will create bootloader config files depending on the presence of
# the following files
ostree_sysroot_bootloader()
{
	case "$BOOTLOADER" in
	"syslinux")
		# ostree will see the syslinux/syslinux.cfg and create
		# loader/syslinux.cfg. The extlinux.conf is hardcoded in
		# NVIDIA U-Boot. They should all point to the same file.
		mkdir -p "$OSTREE_SYSROOT_BOOT"/{syslinux,extlinux}
		# TODO ostree doesn't recognise symlinks, so touch instead
		#ln -s /boot/loader/syslinux.cfg \
		#	"$OSTREE_SYSROOT_BOOT/syslinux/syslinux.cfg"
		touch "$OSTREE_SYSROOT_BOOT/syslinux/syslinux.cfg"

		# TODO maybe U-Boot recognises symlinks? If so, this will let
		# NVIDIA U-Boot access ostree-generated config.
		ln -s /boot/loader/syslinux.cfg \
			"$OSTREE_SYSROOT_BOOT/extlinux/extlinux.conf"
		;;
	"uboot")
		mkdir -p "${OSTREE_SYSROOT_BOOT}/loader.0"
		ln -s loader.0 "${OSTREE_SYSROOT_BOOT}/loader"
		touch "${OSTREE_SYSROOT_BOOT}/loader/uEnv.txt"
		ln -s loader/uEnv.txt "${OSTREE_SYSROOT_BOOT}/uEnv.txt"
		;;
	"bootloaderspec")
		echo "ostree creates bootloaderspec config by default"
		;;
	*)
		pr_err "Unknown bootloader $BOOLOADER"
		return 1
	esac
}

ostree_sysroot_deploy()
{
	# Deploy with root=UUID random
	ostree admin --sysroot="$OSTREE_SYSROOT" deploy \
		--os="$OSNAME" \
		-v \
		"$OSTREE_REMOTE_NAME:$OSTREE_BRANCH"
}

if ! setup; then
	exit 1
fi
ostree_sysroot_init
ostree_sysroot_pull
ostree_sysroot_bootloader
ostree_sysroot_deploy

echo "Deployment is at $OSTREE_SYSROOT"
echo "Write this to disk"
