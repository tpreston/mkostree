#!/bin/bash
# Convert a normal rootfs into ostree-root and commit to repo. A repo is created
# if one does not already exist.
# See: https://ostree.readthedocs.io/en/latest/manual/adapting-existing/

# shellcheck disable=SC2155
declare -r MKOSTREE="$(realpath "$(dirname "$(dirname "$0")")")"

declare -r REPO="$MKOSTREE/repo"
declare -r ROOTFS="$MKOSTREE/rootfs"
declare -r BRANCH="codethink"
declare -r KVER="4.9.140-tegra"
declare -r KERNEL="$ROOTFS/usr/lib/modules/$KVER/vmlinuz"
# If we can't find a kernel, try copying this one
declare -r LOCAL_KERNEL="$MKOSTREE/vmlinuz"

pr_err()
{
	>&2 echo "ERROR: $*"
}

setup()
{
	if [ ! -d "$ROOTFS" ]; then
		echo "$ROOTFS does not exist"
		return 1
	fi
}

repo_create()
{
	if [ -f "$REPO/config" ]; then
		echo "Using existing repo $REPO"
	else
		echo "Initialising $REPO"
		ostree --repo="$REPO" init --mode=archive
	fi
}

repo_commit()
{
	echo "Commit to ostree"
	ostree --repo="$REPO" commit --branch="$BRANCH" "$ROOTFS"
}

rootfs_layout()
{
	# Setup and split out /etc
	# TODO ostree stores information about remotes in /etc and we had a bug
	# in our second deployment where /etc did not persist and we lost the
	# remote. We figured we could iron this out during bst integration so
	# this function will likely need updating (or at least some attention).
	if [ -d "$ROOTFS/usr/etc" ]; then
		pr_err "Non-empty /usr/etc found!"
	fi
	mv "$ROOTFS/etc" "$ROOTFS/usr/etc"

	mkdir -p "$ROOTFS/sysroot"
	ln -s /sysroot/ostree "$ROOTFS/ostree"

	rm -rf "${ROOTFS:?}"/{home,root,media,opt,usr/local}
	ln -s /var/home "$ROOTFS/home"
	ln -s /var/roothome "$ROOTFS/root"
	ln -s /run/media "$ROOTFS/media"
	ln -s /var/opt "$ROOTFS/opt"
	ln -s /var/local "$ROOTFS/usr/local"
}

# The ostree deploy stage copies a kernel from the deployment rootfs into the
# ostree /boot directory.
rootfs_kernel()
{
	declare -r kdir="$(dirname "$KERNEL")"

	if [ -f "$KERNEL" ]; then
		echo "Found kernel $KERNEL"
		return 0
	fi
	if [ -f "$LOCAL_KERNEL" ]; then
		echo "Using kernel $LOCAL_KERNEL"
		mkdir -p "$kdir"
		cp "$LOCAL_KERNEL" "$KERNEL"
		return 0
	fi

	pr_err "Could not install kernel"
	return 1
}

teardown()
{
	echo "Removing rootfs $ROOTFS"
	rm -rf "$ROOTFS"
}

if ! setup; then
	exit 1
fi
if ! rootfs_kernel; then
	exit 1
fi
repo_create
rootfs_layout
repo_commit
teardown
