#!/bin/bash -e
# Create the rootfs. This is a placeholder script, you will want to replace this
# with however you generate your rootfs (I'm copying a rootfs.tar artifact from
# buildroot).

pr_usage()
{
	echo "usage: RTAR=server:buildroot/output/images/rootfs.tar ./$0"
}

if [ -z "$RTAR" ]; then
	echo "RTAR is unset"
	pr_usage
	exit 1
fi
scp "$RTAR" rootfs.tar
mkdir -p rootfs
tar -xf rootfs.tar -C rootfs
