# mkostree
A project to document the creation of a libostree rootfs. I wanted to simplify
the process used by:
- [deb-ostree-builder](https://github.com/dbnicholson/deb-ostree-builder)
- [coreos-assembler](https://github.com/coreos/coreos-assembler)

## Creating a rootfs
```bash
# Create a normal rootfs
./scripts/root_init.sh

# Convert rootfs to ostree rootfs, and commit to repo
sudo ./scripts/root_ostree.sh

# Create an initial deployment
sudo ./scripts/create_deployment.sh
```

# Graphs
I have added a graph to help visualise the boot flow. You will need to install
the graphviz package.
```bash
make -C graphs
```
